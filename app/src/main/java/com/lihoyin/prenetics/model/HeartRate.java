package com.lihoyin.prenetics.model;

public class HeartRate {
    private int rate;
    private String timestamp;

    public HeartRate(int rate, String timestamp) {
        this.rate = rate;
        this.timestamp = timestamp;
    }
}
