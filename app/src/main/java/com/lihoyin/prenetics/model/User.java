package com.lihoyin.prenetics.model;

public class User {
    private String firstname;
    private String lastname;
    private String email;
    private String dob;

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getEmail() {
        return email;
    }

    public String getDob() {
        return dob;
    }
}
