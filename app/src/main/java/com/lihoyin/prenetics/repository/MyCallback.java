package com.lihoyin.prenetics.repository;

import android.content.Context;

import java.lang.ref.WeakReference;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyCallback<T> implements Callback<T> {
    private WeakReference<Context> contextWf;

    public MyCallback(Context context) {
        this.contextWf = new WeakReference<>(context);
    }

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if (contextWf != null && contextWf.get() != null) {
            if (response.body() != null) {
                onSuccess(response.body());
            }
        }
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        onFail(t);
    }

    protected void onSuccess(T result) {

    }

    protected void onFail(Throwable t) {
    }
}
