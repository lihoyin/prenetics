package com.lihoyin.prenetics.repository.service;

import com.lihoyin.prenetics.model.Genotype;
import com.lihoyin.prenetics.model.HeartRate;
import com.lihoyin.prenetics.model.Login;
import com.lihoyin.prenetics.model.Report;
import com.lihoyin.prenetics.model.Token;
import com.lihoyin.prenetics.model.User;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface CustomerService {
    @POST("customer/login")
    Call<Token> login(@Body Login login);

    @POST("customer/logout")
    Call<Token> logout(@Query("customerId") int customerId);

    @GET("customer/{customerId}/user")
    Call<User> getCustomer(@Path("customerId") int customerId);

    @GET("customer/{customerId}/report")
    Call<Report> getHealthReport(@Path("customerId") int customerId);

    @GET("customer/{customerId}/genetic")
    Call<List<Genotype>> getGeneticDetail(@Path("customerId") int customerId);

    @POST("customer/{customerId}/heartrate")
    Call<ResponseBody> submitHeartRate(@Path("customerId") int customerId, @Body HeartRate rate);
}
