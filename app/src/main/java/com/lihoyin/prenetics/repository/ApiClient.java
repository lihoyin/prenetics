package com.lihoyin.prenetics.repository;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.lihoyin.prenetics.BuildConfig;
import com.lihoyin.prenetics.MyApplication;
import com.lihoyin.prenetics.repository.service.CustomerService;
import com.lihoyin.prenetics.util.PrefUtil;

import java.util.concurrent.TimeUnit;

import ir.mirrajabi.okhttpjsonmock.interceptors.OkHttpMockInterceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {
    private static final String API_BASE_URL = "https://api.prenetics.com/v1/";

    private static OkHttpClient okHttpClient = getOkHttpClient();
    private static Gson gson = new GsonBuilder().setLenient().create();

    private final static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(API_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(okHttpClient).build();

    private static OkHttpClient getOkHttpClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(60, TimeUnit.SECONDS);

        if (BuildConfig.FLAVOR.equals("mock")) {
            builder.addInterceptor(new OkHttpMockInterceptor(MyApplication.getContext(), 0));
        }

        builder.addInterceptor(chain -> {
            Request request;

            if (PrefUtil.getToken() != null) {
                request = chain.request().newBuilder()
                        .addHeader("Authorization", "Bearer " + PrefUtil.getToken())
                        .build();
            } else {
                request = chain.request();
            }

            return chain.proceed(request);
        });

        return builder.build();
    }

    public final static CustomerService customerService = retrofit.create(CustomerService.class);
}
