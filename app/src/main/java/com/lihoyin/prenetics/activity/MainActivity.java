package com.lihoyin.prenetics.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.lihoyin.prenetics.R;
import com.lihoyin.prenetics.adapter.GenotypeRvAdapter;
import com.lihoyin.prenetics.lib.heartRateMonitor.HeartRateMonitorActivity;
import com.lihoyin.prenetics.model.Genotype;
import com.lihoyin.prenetics.model.HeartRate;
import com.lihoyin.prenetics.model.Token;
import com.lihoyin.prenetics.repository.ApiClient;
import com.lihoyin.prenetics.repository.MyCallback;
import com.lihoyin.prenetics.util.PrefUtil;
import com.lihoyin.prenetics.util.UiUtil;

import java.util.Date;
import java.util.List;

import okhttp3.ResponseBody;

public class MainActivity extends AppCompatActivity {
    private static final int REQUEST_PERMISSION_CAMERA = 100;
    private static final int REQUEST_HEART_RATE = 101;

    private SwipeRefreshLayout srlGenotype;
    private GenotypeRvAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView rvGenotype = (RecyclerView) findViewById(R.id.rvGenotype);
        srlGenotype = (SwipeRefreshLayout) findViewById(R.id.srlGenotype);

        adapter = new GenotypeRvAdapter();
        rvGenotype.setAdapter(adapter);
        rvGenotype.setLayoutManager(new LinearLayoutManager(this));

        srlGenotype.setRefreshing(true);
        ApiClient.customerService.getGeneticDetail(PrefUtil.getCustomerId()).enqueue(new MyCallback<List<Genotype>>(this) {
            @Override
            protected void onSuccess(List<Genotype> result) {
                srlGenotype.setRefreshing(false);
                srlGenotype.setEnabled(false);

                super.onSuccess(result);
                adapter.setGenoTypes(result);
            }

            @Override
            protected void onFail(Throwable t) {
                super.onFail(t);

                srlGenotype.setRefreshing(false);
                srlGenotype.setEnabled(false);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_HEART_RATE && resultCode == RESULT_OK) {
            int heartRate = data.getIntExtra(HeartRateMonitorActivity.EXTRA_HEART_RATE, 0);
            ApiClient.customerService.submitHeartRate(PrefUtil.getCustomerId(), new HeartRate(heartRate, new Date().toString())).enqueue(new MyCallback<ResponseBody>(this) {
                @Override
                protected void onSuccess(ResponseBody result) {
                    Toast.makeText(MainActivity.this, getText(R.string.heartRateSubmitted), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_logout) {
            doLogout();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] results) {
        if (requestCode == REQUEST_PERMISSION_CAMERA && results.length > 0 && results[0] == PackageManager.PERMISSION_GRANTED) {
            startHeartRateMonitor();
        }
    }

    public void onHeartClicked(View view) {
        startHeartRateMonitor();
    }

    public void onViewReportClicked(View view) {
        startActivity(new Intent(this, ReportActivity.class));
    }

    private void startHeartRateMonitor() {
        if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CAMERA}, REQUEST_PERMISSION_CAMERA);
        } else {
            startActivityForResult(new Intent(this, HeartRateMonitorActivity.class), REQUEST_HEART_RATE);
        }
    }

    private void doLogout() {
        UiUtil.showConfirmAlert(this, getString(R.string.confirm_to_logout), getString(R.string.logout),
                (dialogInterface, i) -> ApiClient.customerService.logout(PrefUtil.getCustomerId()).enqueue(new MyCallback<Token>(MainActivity.this) {
                    @Override
                    protected void onSuccess(Token result) {
                        PrefUtil.logout();
                        startActivity(new Intent(MainActivity.this, LoginActivity.class));
                        finish();
                    }
                }));
    }
}
