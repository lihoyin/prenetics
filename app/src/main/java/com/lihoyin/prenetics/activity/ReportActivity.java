package com.lihoyin.prenetics.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.lihoyin.prenetics.R;
import com.lihoyin.prenetics.model.Report;
import com.lihoyin.prenetics.repository.ApiClient;
import com.lihoyin.prenetics.repository.MyCallback;
import com.lihoyin.prenetics.util.PrefUtil;

public class ReportActivity extends AppCompatActivity {
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        WebView webView = (WebView) findViewById(R.id.webView);
        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);


        webView.setWebViewClient(new WebViewClient() {

            public void onPageFinished(WebView view, String url) {
                progressBar.setVisibility(View.GONE);
            }
        });

        ApiClient.customerService.getHealthReport(PrefUtil.getCustomerId()).enqueue(new MyCallback<Report>(this) {
            @Override
            protected void onSuccess(Report result) {
                webView.loadUrl(result.getUrl());
            }
        });
    }
}
