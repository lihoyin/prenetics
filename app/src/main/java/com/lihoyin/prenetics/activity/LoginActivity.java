package com.lihoyin.prenetics.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.lihoyin.prenetics.R;
import com.lihoyin.prenetics.model.Login;
import com.lihoyin.prenetics.model.Token;
import com.lihoyin.prenetics.repository.ApiClient;
import com.lihoyin.prenetics.repository.MyCallback;
import com.lihoyin.prenetics.util.PrefUtil;

public class LoginActivity extends AppCompatActivity {
    private EditText etUsername;
    private EditText etPassword;
    private Button btnLogin;

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            btnLogin.setEnabled(isInputValid());
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etUsername = (EditText) findViewById(R.id.etUserName);
        etPassword = (EditText) findViewById(R.id.etPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);

        etUsername.addTextChangedListener(textWatcher);
        etPassword.addTextChangedListener(textWatcher);
    }

    public void onLoginClicked(View view) {
        if (!isInputValid()) {
            return;
        }

        ApiClient.customerService.login(new Login(etUsername.getText().toString(), etPassword.getText().toString())).enqueue(new MyCallback<Token>(this) {
            @Override
            protected void onSuccess(Token result) {
                PrefUtil.login();
                PrefUtil.setToken(result.getToken());
                PrefUtil.setCustomerId(1); //TODO: Login API should also provide customerId

                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                finish();
            }
        });
    }

    private boolean isInputValid() {
        if (TextUtils.isEmpty(etUsername.getText()) || TextUtils.isEmpty(etPassword.getText())) {
            return false;
        }

        return true;
    }
}
