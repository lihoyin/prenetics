package com.lihoyin.prenetics.util;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.lihoyin.prenetics.MyApplication;

public class PrefUtil {
    public static String IS_LOGINED = "IS_LOGINED";
    public static String TOKEN = "TOKEN";
    public static String CUSTOMER_ID = "CUSTOMER_ID";

    public static void login() {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(MyApplication.getContext()).edit();
        editor.putBoolean(IS_LOGINED, true);
        editor.commit();
    }

    public static void logout() {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(MyApplication.getContext()).edit();
        editor.clear();
        editor.commit();
    }

    public static boolean isLogined() {
        return PreferenceManager.getDefaultSharedPreferences(MyApplication.getContext()).getBoolean(IS_LOGINED, false);
    }

    public static void setToken(String token) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(MyApplication.getContext()).edit();
        editor.putString(TOKEN, token);
        editor.commit();
    }

    public static String getToken() {
        return PreferenceManager.getDefaultSharedPreferences(MyApplication.getContext()).getString(TOKEN, null);
    }

    public static void setCustomerId(int customerId) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(MyApplication.getContext()).edit();
        editor.putInt(CUSTOMER_ID, customerId);
        editor.commit();
    }

    public static int getCustomerId() {
        return PreferenceManager.getDefaultSharedPreferences(MyApplication.getContext()).getInt(CUSTOMER_ID, 0);
    }
}
