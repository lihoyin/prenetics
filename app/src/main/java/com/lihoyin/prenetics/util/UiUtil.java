package com.lihoyin.prenetics.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.lihoyin.prenetics.R;

/**
 * Created by lihoyin on 12/11/2017.
 */

public class UiUtil {
    public static void showAlert(Context context, String message) {
        new AlertDialog.Builder(context)
                .setTitle(message)
                .setPositiveButton(context.getText(R.string.confirm), (dialogInterface, i) -> {
                }).create().show();
    }

    public static void showConfirmAlert(Context context, String message, String confirmButtonText, DialogInterface.OnClickListener onClickListener) {
        new AlertDialog.Builder(context)
                .setTitle(message)
                .setNegativeButton(context.getText(R.string.cancel), null)
                .setPositiveButton(confirmButtonText, onClickListener).create().show();
    }
}
