package com.lihoyin.prenetics.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lihoyin.prenetics.R;
import com.lihoyin.prenetics.model.Genotype;

import java.util.ArrayList;
import java.util.List;

public class GenotypeRvAdapter extends RecyclerView.Adapter<GenotypeRvAdapter.ViewHolder> {
    private final List<Genotype> genotypes = new ArrayList<>();

    @Override
    public GenotypeRvAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_genotype, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Genotype genotype = genotypes.get(position);
        holder.tvName.setText(genotype.getName());
        holder.tvSymbol.setText(genotype.getSymbol());
    }

    @Override
    public int getItemCount() {
        return genotypes.size();
    }

    public void setGenoTypes(List<Genotype> genotypes) {
        this.genotypes.clear();
        this.genotypes.addAll(genotypes);

        notifyDataSetChanged();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvName;
        TextView tvSymbol;

        ViewHolder(View v) {
            super(v);

            tvName = v.findViewById(R.id.tvName);
            tvSymbol = v.findViewById(R.id.tvSymbol);
        }
    }
}
