package com.lihoyin.prenetics;

import android.app.Instrumentation;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.lihoyin.prenetics.activity.LoginActivity;
import com.lihoyin.prenetics.activity.MainActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isEnabled;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static junit.framework.Assert.assertNotNull;
import static org.hamcrest.Matchers.not;


@RunWith(AndroidJUnit4.class)
@LargeTest
public class LoginValidationTest {

    @Rule
    public ActivityTestRule<LoginActivity> mActivityRule = new ActivityTestRule<>(LoginActivity.class);

    @Test
    public void empty_username() {
        // Login button disabled when no username inputted
        onView(withId(R.id.etUserName)).perform(typeText(""));
        onView(withId(R.id.etPassword)).perform(typeText("pass"));
        onView(withId(R.id.btnLogin)).check(matches(not(isEnabled())));
    }

    @Test
    public void empty_password() {
        // Login button disabled when no password inputted
        onView(withId(R.id.etUserName)).perform(typeText("TestName"));
        onView(withId(R.id.etPassword)).perform(typeText(""));
        onView(withId(R.id.btnLogin)).check(matches(not(isEnabled())));
    }

    @Test
    public void completed_form() {
        // Login button enable when username and password inputted
        onView(withId(R.id.etUserName)).perform(typeText("TestName"));
        onView(withId(R.id.etPassword)).perform(typeText("pass"));
        onView(withId(R.id.btnLogin)).check(matches(isEnabled()));

        // MainActivity opened when login success
        Instrumentation.ActivityMonitor activityMonitor = getInstrumentation().addMonitor(MainActivity.class.getName(), null, false);
        onView(withId(R.id.btnLogin)).perform(click());
        MainActivity targetActivity = (MainActivity) activityMonitor.waitForActivity();
        assertNotNull(targetActivity);
    }
}
