package com.lihoyin.prenetics;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.lihoyin.prenetics.TestUtil.RecyclerViewItemCountAssertion;
import com.lihoyin.prenetics.activity.MainActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.isEnabled;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.lihoyin.prenetics.TestUtil.atPosition;


@RunWith(AndroidJUnit4.class)
@LargeTest
public class GenotypeListTest {
    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<MainActivity>(MainActivity.class) {
        @Override
        protected void beforeActivityLaunched() {
            SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(MyApplication.getContext()).edit();
            editor.putBoolean("IS_LOGINED", true);
            editor.putInt("CUSTOMER_ID", 1);
            editor.commit();

            super.beforeActivityLaunched();
        }
    };

    @Test
    public void list() {
        onView(withId(R.id.fab)).check(matches(isEnabled()));

        // List contain 3 item
        onView(withId(R.id.rvGenotype)).check(new RecyclerViewItemCountAssertion(3));

        // List data
        onView(withId(R.id.rvGenotype)).check(matches(atPosition(0, hasDescendant(withText("eye color")))));
        onView(withId(R.id.rvGenotype)).check(matches(atPosition(0, hasDescendant(withText("Xx")))));

        onView(withId(R.id.rvGenotype)).check(matches(atPosition(1, hasDescendant(withText("hair color")))));
        onView(withId(R.id.rvGenotype)).check(matches(atPosition(1, hasDescendant(withText("xx")))));


        onView(withId(R.id.rvGenotype)).check(matches(atPosition(2, hasDescendant(withText("hepatitis-B virus")))));
        onView(withId(R.id.rvGenotype)).check(matches(atPosition(2, hasDescendant(withText("XX")))));
    }
}
